con <- dbConnect(dbDriver("SQLite"), dbname = "cpsdb")
query <- dbSendQuery(con,"select * from main where year=1979")
ipums79 <- fetch(query,n=-1)
load("../yearData/cps1979.Rdata")
## remove underscores from beginning of variable names
names(cpsyear) <- sub("^_+","",names(cpsyear))

## sample selection for Mulligan and Rubinstein
mrSample <- subset(cpsyear, age>=26 & age<=55 & wageworker==1 &
                   race=="White" & hispanic==0 &
                   group_quarters=="Housing unit" &
                   state!=25 & state!=27 & agriculture==0 &
                   popstat=="CivAdlt" & phh==0 & allocated==0 & selfemp==0 &
                   !is.na(wage_cpi) & wage_cpi!=0 & !is.na(wgt)) 

## do the same for the IPUMS
names(ipums79) <- tolower(names(ipums79))

ipums79$allocated <-
  rowSums(ipums79[,c("qhrswork","qincwage")])>0
ipums79$wage.v1 <- ipums79$incwage/(ipums79$uhrswork*ipums79$wkswork1)*ipums79$cpi99
ipums79$wage.v2 <- ipums79$incwage/(ipums79$hrswork*ipums79$wkswork1)*ipums79$cpi99


mrIpums <- subset(ipums79,  age>=26 & age<=55 &                
                  race==100 & ## only whites 
                  gq==1 &  ## group quarters
                  statefip!=25 & statefip!=27 & ## MA & MN !?
                  ind50ly!=105 & ## no agriculture
                  popstat==1 & ## civilians
                  classwly!=10 & classwly!=13 & classwly!=14 & ## no self-employed
                  allocated==0)



## FIXES:
##  - no measure of hispanic
##  - OCC60LY no needed
##  - what is "wageworker"? paidhour does not seem to match , maybe
##         classwly==20
##  - state!=25 and state!=27 is MA and MN if state is statefips
##  - agriculture based on ind50ly? -- yes looks like it
##  - what is phh?
##  - how to construct wage?
##    - 14.32:  Angrist Usual weekly earnings = incwage/wkswork1
##    - MR: weekly_wage = wage_cpi / wksly
##          hourly_wage = wage_cpi / hourly_wage
##      ? what is wage_cpi and how does it realte to incwag_cpi ?
