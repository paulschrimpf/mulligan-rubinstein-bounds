#!/bin/perl 
my $N = 2; # number of processes to span
my $reps = 50; # number of repetitions per process
my $script = 'mulliganRubinstein.R'; # script to run

my $n=0;
while($n<$N) {
    my $pid = fork();
    if (not defined $pid) {
        die "resources not avilable\n";
    } elsif ($pid == 0) {
        my $r;
        for($r=0;$r<$reps;$r++) {
            `nice R --vanilla < $script &> rOut$n\_$r.txt`;
        }
        exit(0);
    } else {
        $n++;
        print "Spawned $n R processes\n";
        sleep 2;
    }
}
