### Produces empirical results from Chandrasekhar, Chernozhukov,
### Molinari and Schrimpf. Based on table 1 of Mulligan and
### Rubinstein. Produces both Mulligan and Rubinstein's estimates and
### a quantile-bounds version.

### To produce point estimates set bootstrap to FALSE. Output will be
### in a file callled `nameroot'.csv. For bootstrap, set bootstrap to
### TRUE. Each time this script is run, it completes one bootstrap
### replication and stores results in bs/nameroot%d.csv. To run many
### replications use the perl script manyR.pl. Use the Octave script
### plotBands.m to process the results and generate the figures and
### tables in the paper.

## FIXME: everything could and should be done in R

### Substantial memory is required to run this script. The dataset is
### large, and many R objects involve making copies of substantial
### portion of the data. Objects are deleted when not needed to save
### on memory, but it still requires approximately 1.5Gb.

##  Rprof("prof.out")
rm(list=ls())
bootstrap <- FALSE
nameroot <- "april2021"
if (bootstrap) {
  for(j in 1:1000) {
    filename <- paste("bs/",nameroot,j,".csv",sep="")
    if (!file.exists(filename)) {
      break
    }
    if (j==1000) {
      print("crap\n")
      filename <- NULL
    }
  }
} else {
  filename <- paste(nameroot,".csv",sep="")
}
file.create(filename)
source("selectionQuantiles.R",echo=FALSE)
source("myrq.R",echo=FALSE)

allData <- read.csv(gzfile("table1Data.csv.gz",open="r"),
                    header = TRUE, sep = ",",
                    quote="\"", dec=".", #"
                    fill = TRUE)
synData <- allData[allData$syntheticobs==1,]
realData <- allData[allData$syntheticobs==0,]
drops = c("wgt","X_state","occlyr","agely","hsd08","hsd911","hsg","sc","cg","ad",
  "exp1_hsd08", "exp1_hsd911", "exp1_hsg", "exp1_cg", "exp1_ad", "exp2_hsd08", "exp2_hsd911",
  "exp2_hsg", "exp2_cg", "exp2_ad", "exp3_hsd08", "exp3_hsd911",
  "exp3_hsg", "exp3_cg", "exp3_ad", "exp4_hsd08", "exp4_hsd911",
  "exp4_hsg", "exp4_cg", "exp4_ad", "agelyr", "age40",
  "age40sq", "age40cu", "age40qu")


data <- realData[,!(colnames(realData) %in% drops)]
if (bootstrap) { ## exponential weights
  data$ei <- rexp(nrow(data),1)
  data$ei <- data$ei/mean(data$ei)
} else { ## set weights to one
  data$ei <- rep(1,nrow(data))
}
rm(list=c("allData","realData","synData"))

## memory info
print(lsos())
##experience quadratic.  encode educ as years and use polynomial
yrseduc <- NA*as.numeric(data$education)
yrseduc[data$education=="Advanced Degree"] <- 18
yrseduc[data$education=="College Gradaute"] <- 16 # (sic)
yrseduc[data$education=="Some College"] <- 14
yrseduc[data$education=="HSG"] <- 12
yrseduc[data$education=="HSD 11"] <- 11
yrseduc[data$education=="HSD 10"] <- 10
yrseduc[data$education=="HSD 9"] <- 9
yrseduc[data$education=="HSD 8 or less"] <- 8
data$yrseduc <- yrseduc
##data$childsq <- data$X__child06*data$X__child06
tmp <- 8*data$hsd08 + 10*data$hsd911 + 12*data$hsg + 16*data$cg + 18*data$ad +
  14*(1-data$hsd08-data$hsd911-data$hsg -data$cg-data$ad)
data$yrseduc[data$syntheticobs==1] <- tmp[data$syntheticobs==1]
data$yrseduc2 <- data$yrseduc*data$yrseduc/10

data$single <- data$widowed+data$divorced+data$separated+data$nevermarried
data$wasmarried <- data$widowed+data$divorced+data$separated
rm(list="yrseduc","tmp")

data$child0 <- data$X__child06==0
data$child1 <- data$X__child06==1
data$child2 <- data$X__child06==2
data$child3 <- data$X__child06==3
data$child4 <- data$X__child06==4
data$child5p <- data$X__child06>=5
## create instrument : number of children interacted with marital status
nchild <- round(data$X__child06)
nchild[nchild>5] <- 5 ## truncate at 5 for married to avoid small cells
nchild3 <- nchild
nchild3[nchild>3] <- 3 ## truncate at 3 for not married to avoid small cells
## data$Z <- as.factor(nchild*(1-data$wasmarried-data$nevermarried) +
##                     (20+nchild3)*data$wasmarried +
##                     (30+nchild3)*data$nevermarried)
sHiEd <- ((data$wasmarried==1 | data$nevermarried==1) & data$yrseduc>=16)
nchild3[sHiEd & nchild3>1] <- 1  # use only 0 or 1+ children for
                                 # single, high educated because very
                                 # few have more than 1 child
data$nchild <- nchild
data$Z <- as.factor(nchild3)
rm(list=c("nchild","nchild3","sHiEd"))
##data$childsq <- data$X__child06*data$X__child06


## formula for outcome equation
wfmla <- (lnw ~ female*(wasmarried + nevermarried +
                        yrseduc + exp1+exp2 + mw + so + we) )
## used for bounding functions
data$hied <- data$yrseduc>=16
data$loed <- data$yrseduc<=12
wfmlaz <- (lnw ~ female*
             (wasmarried + nevermarried + yrseduc + yrseduc2
               + exp1+exp2+exp3 + yrseduc:exp1 + mw + so + we)
              + female*(wasmarried+nevermarried)*Z)
## formula for selection equation (probit)
sfmla <- (ftfy50  ~ yrseduc + exp1 + exp2 + mw + so + we +
          (wasmarried + nevermarried)*(child0+child1+
                                       child3+child4+child5p))
data$subgroup <- data$yrseduc>=0
## report wage gap for average female within these groups
groups <- cbind(data$yrseduc>=0, ## all
                data$wasmarried==1 | data$nevermarried==1, ## single
                data$yrseduc<=12, ## low education
                data$yrseduc>=16, ## high education
                (data$wasmarried==1 | data$nevermarried==1) & data$yrseduc>=16) ## high education and single
#data$groups <- groups
## for average female x at which to report things
ngroup <- ncol(groups)
syndata <- data.frame()
sg <- NULL
for (g in 1:ngroup) {
  meanData <- data[1:4,]
  meanData[1,] <- sapply(data[groups[,g] & data$female==1 & data$ftfy50==1,],
                          function(x) mean(x, na.rm=TRUE))
  for (r in 2:4) {
    meanData[r,]  <- meanData[1,]
  }
  meanData$syntheticobs <- rep(g,4)
  meanData$wkyr <- c(1975,1975,1995,1995)
  meanData$female <- c(1,0,1,0)
  meanData$lnw <- c(NA,NA,NA,NA)
  meanData$wagesmpl <- c(NA,NA,NA,NA)
  grp <- rep(FALSE, ngroup)
  grp[g] <- TRUE
  syndata <- rbind(syndata,meanData)
  sg <- rbind(sg, matrix(rep(grp,4),4,byrow=TRUE))
}
data <- rbind(data, syndata)
groups <- rbind(groups,sg)

rm(list=c("syndata","meanData"))
 ## for average female x at which to report things
taus <- 0.5 # changed in loop below
algo = "fn" # or br or pfn
#################################################################################
### END OF SETTINGS
#################################################################################

gapols <- NULL
gapheck <- NULL
norm <- function(x) { return(sqrt(sum(x^2))) }
#stop
for (g in 1:ngroup) {
  data$subgroup <- groups[,g]
  ## probit for selection
  select <- glm(sfmla, family=binomial(link="probit"), data = data,
                subset = (female==1 & wkyr>=1975 & wkyr<=1979 & subgroup),
                weights=ei)
  xb <-  predict(select,newdata=data)
  select <- glm(sfmla, family=binomial(link="probit"), data = data,
                subset = (female==1 & wkyr>=1995 & wkyr<=1999 & subgroup),
                weights=ei)
  xb[data$wkyr>=1995 & data$wkyr<=1999] <-
    predict(select,newdata=data)[data$wkyr>=1995 & data$wkyr<=1999]
  pHat <- pnorm(xb)
  mills <- dnorm(xb)/pHat
  pHat[data$female==0 | data$syntheticobs>0] <- 1
  mills[data$female==0 | data$syntheticobs>0] <- 0
  data$mills <- mills

  ## OLS
  ols75 <- lm(wfmla, data=data, subset=(trim==0 & ftfy50==1 & wagesmpl==1
                       & wkyr>=1975 & wkyr<=1979 & subgroup),weights=ei)
  ols95 <- lm(wfmla, data=data, subset=(trim==0 & ftfy50==1 & wagesmpl==1
                       & wkyr>=1995 & wkyr<=1999 & subgroup),weights=ei)
  ## Heckit
  heck75 <- lm(update(wfmla,. ~ . + female:mills), data=data,
               subset=(trim==0 & ftfy50==1 & wagesmpl==1 & wkyr>=1975 &
                       wkyr<=1979 & subgroup),weights=ei)
  heck95 <- lm(update(wfmla,. ~ . + female:mills), data=data,
               subset=(trim==0 & ftfy50==1 & wagesmpl==1 & wkyr>=1995 &
                       wkyr<=1999 & subgroup),weights=ei)

  ols <- c(predict(ols75,newdata=data)[data$syntheticobs==g &
                           data$wkyr==1975 & data$female==0],
           predict(ols75,newdata=data)[data$syntheticobs==g &
                           data$wkyr==1975 & data$female==1],
           predict(ols95,newdata=data)[data$syntheticobs==g &
                           data$wkyr==1995 & data$female==0],
           predict(ols95,newdata=data)[data$syntheticobs==g &
                                       data$wkyr==1995 & data$female==1])
  gapols <- c(gapols,ols[2]-ols[1],ols[4]-ols[3])
  heck <- c(predict(heck75,newdata=data)[data$syntheticobs==g &
                             data$wkyr==1975 & data$female==0],
            predict(heck75,newdata=data)[data$syntheticobs==1 &
                             data$wkyr==1975 & data$female==1],
            predict(heck95,newdata=data)[data$syntheticobs==g &
                             data$wkyr==1995 & data$female==0],
            predict(heck95,newdata=data)[data$syntheticobs==g &
                             data$wkyr==1995 & data$female==1])
  gapheck <- c(gapheck,heck[2]-heck[1],heck[4]-heck[3])
}

names(gapols) <- as.vector(sapply(1:ngroup,function(x) c(sprintf("ols g%d 75-79",x),
                                                         sprintf("ols g%d 95-99",x))))
names(gapheck) <- as.vector(sapply(1:ngroup,function(x) c(sprintf("heck g%d 75-79",x),
                                                          sprintf("heck g%d 95-99",x))))

line <- c(-1,gapols,gapheck)
write(line,
      file=filename,ncolumns=length(line),append=FALSE,sep=",")
rm(list=c("select","ols","heck","mills",
        "pHat","xb","allData","realData",
        "ols75","ols95","heck75","heck95","gapols","gapheck"))

## memory info
gc()
print(lsos())

## quantile regression
library(quantreg)

##taus <- seq(0.1,0.9,0.05)
#for(taus in seq(0.1,0.9,0.1)) {
for(taus in seq(0.5,0.5,0.1)) {
  gapqr <- NULL
  for (g in 1:ngroup) {
    data$subgroup <- groups[,g]
    ## quantile regression
    data$sub7 <- data$trim==0 & ((data$ftfy50==1 & data$wagesmpl==1) |
                   data$female==1) & data$wkyr>=1975 & data$wkyr<=1979 & data$subgroup==1
    data$sub9 <- data$trim==0 & ((data$ftfy50==1 & data$wagesmpl==1) |
                   data$female==1) & data$wkyr>=1995 & data$wkyr<=1999 & data$subgroup==1
    qr75 <- myrq(wfmla, data=data, subset=data$sub7,
                 tau = taus, method=algo,weights=ei)
    qr95 <- myrq(wfmla, data=data, subset=data$sub9,
                 tau = taus, method=algo,weights=ei)
    qr <- cbind(mypredict(qr75,newdata=data)[data$syntheticobs==g &
                                 data$wkyr==1975 & data$female==0],
                mypredict(qr75,newdata=data)[data$syntheticobs==g &
                                 data$wkyr==1975 & data$female==1],
                mypredict(qr95,newdata=data)[data$syntheticobs==g &
                                 data$wkyr==1995 & data$female==0],
                mypredict(qr95,newdata=data)[data$syntheticobs==g &
                               data$wkyr==1995 & data$female==1])
    gapqr <- cbind(gapqr,qr[,2]-qr[,1],qr[,4]-qr[,3])
  }

  rm(list=c("qr75","qr95","qr"))

  ## quantile bounds with selection
  hi7 <- NULL
  lo7 <- NULL
  hi7sd <- NULL
  lo7sd <- NULL
  hi9 <- NULL
  lo9 <- NULL
  hi9sd <- NULL
  lo9sd <- NULL
  for (g in 1:ngroup) {
    data$subgroup <- groups[,g]
    ## quantile regression
    data$sub7 <- data$trim==0 & ((data$ftfy50==1 & data$wagesmpl==1) |
                   data$female==1) & data$wkyr>=1975 & data$wkyr<=1979 & data$subgroup==1
    data$sub9 <- data$trim==0 & ((data$ftfy50==1 & data$wagesmpl==1) |
                   data$female==1) & data$wkyr>=1995 & data$wkyr<=1999 & data$subgroup==1
    ## selection qr
    y0t <- data$lnw
    y0t[data$female==1 & data$ftfy50==0] <- min(data$lnw[data$female==1 & data$ftfy50==1
                           & data$wagesmpl==1 & data$trim==0 & !is.na(data$lnw)])
    y1t <- data$lnw
    y1t[data$female==1 & data$ftfy50==0] <- max(data$lnw[data$female==1 & data$ftfy50==1
                           & data$wagesmpl==1 & data$trim==0 & !is.na(data$lnw)])
    ## 70s
    bounds7 <- selectionQuantileBounds(wfmla,wfmlaz,exclusion.levels=levels(data$Z),exclusion.name="Z"
                                       ,y0=y0t,y1=y1t,
                                       tau=taus,data=data,
                                       subset=data$sub7,weights=ei)
    bsd7 <- selectionQuantileBounds(wfmla,wfmlaz,exclusion.levels=levels(data$Z),exclusion.name="Z",
                                    y0=y0t,y1=data$lnw,
                                    tau=taus,data=data,
                                    subset=data$sub7,weights=ei)
    #t0 <- bounds7$q0x
    #foo <- model.frame(update(wfmla, t0 ~ . ),na.action=na.pass,data=data)
    foo  <- model.frame(wfmla,na.action=na.pass,data=data)
    xAll <-  model.matrix(attr(foo,"terms"),foo)
    q <- xAll[data$syntheticobs==g & data$wkyr==1975 & data$female==1,] -
      xAll[data$syntheticobs==g & data$wkyr==1975 & data$female==0,]
    nq <- norm(q)
    q <- q[sapply(names(q),function(xi) {any(xi==colnames(bounds7@x))})]
    q <- matrix(q*(nq/norm(q)),nrow=length(q),ncol=1)
    hi7 <- cbind(hi7,t(bounds7@s(q)))
    lo7 <- cbind(lo7,t(-bounds7@s(-q)))
    hi7sd <- cbind(hi7sd,t(bsd7@s(q)))
    lo7sd <- cbind(lo7sd,t(-bsd7@s(-q)))
    rm(list=c("foo","t0"))

    ## 90s
    bounds9 <- selectionQuantileBounds(wfmla,wfmlaz,exclusion.levels=levels(data$Z),exclusion.name="Z",
                                       y0=y0t,y1=y1t,
                                       tau=taus,data=data,
                                       subset=data$sub9,weights=ei)
    bsd9 <- selectionQuantileBounds(wfmla,wfmlaz,exclusion.levels=levels(data$Z),exclusion.name="Z",
                                    y0=y0t,y1=data$lnw,
                                    tau=taus,data=data,
                                    subset=data$sub9,weights=ei)
    q <- xAll[data$syntheticobs==g & data$wkyr==1995 & data$female==1,] -
      xAll[data$syntheticobs==g & data$wkyr==1995 & data$female==0,]
    nq <- norm(q)
    q <- q[sapply(names(q),function(xi) {any(xi==colnames(bounds9@x))})]
    q <- matrix(q*(nq/norm(q)),nrow=length(q),ncol=1)
    hi9 <- cbind(hi9,t(bounds9@s(q)))
    lo9 <- cbind(lo9,t(-bounds9@s(-q)))
    hi9sd <- cbind(hi9sd,t(bsd9@s(q)))
    lo9sd <- cbind(lo9sd,t(-bsd9@s(-q)))
    print(sprintf("Finished group %d, tau=%f",g,taus))
  }
  names(gapqr) <- as.vector(sapply(1:ngroup,function(x)
    c(sprintf("qr g%d 75-79",x),
      sprintf("qr g%d 95-99",x))))
  names(lo7) <- as.vector(sapply(1:ngroup,function(x)
    c(sprintf("lo7 g%d 75-79",x),
      sprintf("lo7 g%d 95-99",x))))
  names(hi7) <- as.vector(sapply(1:ngroup,function(x)
    c(sprintf("hi7 g%d 75-79",x),
      sprintf("hi7 g%d 95-99",x))))
  names(lo9) <- as.vector(sapply(1:ngroup,function(x)
    c(sprintf("lo9 g%d 75-79",x),
      sprintf("lo9 g%d 95-99",x))))
  names(hi9) <- as.vector(sapply(1:ngroup,function(x)
    c(sprintf("hi9 g%d 75-79",x),
      sprintf("hi9 g%d 95-99",x))))


  line <- cbind(taus,gapqr,lo7,hi7,lo9,hi9,lo7sd,hi7sd,lo9sd,hi9sd)
  write(line,
        file=filename,ncolumns=ncol(line),
        append=TRUE,sep=" , ")

  ## memory info
  print(lsos())

}
##Rprof(NULL)
